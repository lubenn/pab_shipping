<?php

/**
 * Implements hook_menu().
 */
function pab_shipping_menu() {

  $items['admin/pab/postcode'] = array(
    'title' => 'Postcode Management',
    'page callback' => 'pab_shipping_postcode_list',
    'access arguments' => array('administer postcodes'),
    'description' => t('Manage postcodes you deliver to.'),
    '#weight' => 50,
  );

  $items['admin/pab/postcode/view'] = array(
    'title' => 'Postcodes',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    '#weight' => 50,
  );

  $items['admin/pab/postcode/upload'] = array(
    'title' => 'Upload Postcodes',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pab_shipping_postcode_upload'),
    'access arguments' => array('administer postcodes'),
    'type' => MENU_LOCAL_ACTION,
  );

  $items['admin/pab/postcode/%pab_shipping/edit'] = array(
    'title' => 'Edit Postcode',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pab_shipping_postcode_edit', 3),
    'access arguments' => array('administer postcodes'),
  );

  $items['admin/pab/postcode/%pab_shipping/delete'] = array(
    'title' => 'Edit Postcode',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pab_shipping_postcode_delete', 3),
    'access arguments' => array('administer postcodes'),
  );

  return $items;
}

function pab_shipping_load($postcode) {
  $query = db_select('pab_postcodes', 'p');
  $query->condition('p.postcode', $postcode);
  $query->fields('p');
  $result = $query->execute()->fetchObject();
  return $result;
}

/**
 * Implements hook_permission().
 */
function pab_shipping_permission() {
  return array(
    'administer postcodes' =>  array(
      'title' => t('Administer postcodes'),
      'description' => t('Administer postcodes'),
    ),
  );
}

function pab_shipping_postcode_edit($form, &$form_state, $postcode) {
  $form = array();
  $default_currency = commerce_currency_load();
  $form['postcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Postcode'),
    '#disabled' => TRUE,
    '#default_value' => isset($postcode) ? strtoupper($postcode->postcode) : '',
    '#required' => TRUE,
  );

  $form['price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#default_value' => isset($postcode) ? commerce_currency_amount_to_decimal($postcode->price, $default_currency['code']) : '',
    '#input_group' => TRUE,
    '#field_prefix' => '',
    '#field_suffix' => $default_currency['code'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  if($postcode) {
    $form['delete']['#markup'] = l(t('Delete'), url('admin/pab/postcode/'.$postcode->postcode.'/delete'), array('attributes' => array('class' => array('btn', 'btn-danger'))));
  }
  return $form;
}

function pab_shipping_postcode_edit_submit($form, &$form_state) {
  $postcode = isset($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0]->postcode : false;
  if($postcode) {
    db_merge('pab_postcodes')
      ->key(array('postcode' => $postcode))
      ->fields(array(
          'price' => $form_state['values']['price'] * 100,
      ))
      ->execute();
  } else {
     db_insert('pab_postcodes')
      ->fields(array(
        'postcode' => $form_state['values']['postcode'],
        'price' => $form_state['values']['price'] * 100,
      ))
      ->execute();
  }

  $form_state['redirect'] = 'admin/pab/postcode';
}

function pab_shipping_postcode_delete($form, &$form_state, $postcode) {
  $form_state['postcode'] = $postcode;
  $form['#submit'][] = 'pab_shipping_postcode_delete_form_submit';
  $form = confirm_form($form,
    t('Are you sure you want to delete this postcode?'),
    'admin/pab/postcode',
    '',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  return $form;
}

function pab_shipping_postcode_delete_form_submit($form, &$form_state) {
  $postcode = $form_state['postcode'];
  db_delete('pab_postcodes')->condition('postcode', $postcode->postcode)->execute();
  drupal_set_message(t('The postcode has been deleted.'));
  $form_state['redirect'] = 'admin/pab/postcode';
}

function pab_shipping_postcode_list() {
  $headers = array(t('Postcode'),t('Price'), '');
  $rows = array();

  $default_currency = commerce_currency_load();
  $query = db_select('pab_postcodes', 'p');
  $query->orderBy('p.postcode');
  $query->fields('p');
  $results = $query->execute()->fetchAll();
  foreach($results as $result) {
    $rows[] = array(strtoupper($result->postcode), commerce_currency_format($result->price, $default_currency['code']), l(t('edit'), url('admin/pab/postcode/'.$result->postcode.'/edit')));
  }
  $table_data = array(
      'header'     => $headers,
      'rows'       => $rows,
      'sticky'     => TRUE,
      'empty'      => t('No results found'),
      'attributes' => array('class' => array('table')),
      'caption'    => '',
      'colgroups'  => array()
    );

  return theme_table($table_data);
}

function pab_shipping_postcode_upload($form, &$form_state) {
  $form = array();

  if(!isset( $form_state['storage']['confirm'] ) ) {
    $path = drupal_get_path('module', 'pab_shipping');
    $form['description']['#markup'] = t('Upload a csv file containing postcodes you cover and the delivery price. <a href="@sample">Click here to download a sample</a>', array('@sample' => '/'.$path.'/sample.csv'));

    $form['csv'] = array(
      '#title' => t('Upload'),
      '#name' => 'csv_upload',
      '#type' => 'managed_file',
      '#description' => t('A CSV of postcodes'),
      '#required' => TRUE,
      '#upload_validators' => array(
        'file_validate_extensions' => array('csv'),
      ),
    );

    $form['confirm'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
    );

  } else {
    $headers = array(t('Postcode'),t('Price'));
    $rows = array();
    $default_currency = commerce_currency_load();
    foreach($form_state['storage']['values']['postcodes'] as $postcode => $price) {
      $rows[] = array($postcode, commerce_currency_format($price * 100, $default_currency['code']));
    }

    $table_data = array(
      'header'     => $headers,
      'rows'       => $rows,
      'sticky'     => TRUE,
      'empty'      => t('No results found'),
      'attributes' => array('class' => array('table')),
      'caption'    => '',
      'colgroups'  => array()
    );

    $form['intro']['#markup'] = theme_table($table_data);
    return confirm_form($form, t('Confirmation'), 'admin/pab/postcode', t('Are you sure you want to upload these postcodes?'), t("Save"));
  }

  return $form;
}

function pab_shipping_postcode_upload_submit($form, &$form_state) {
  if(!isset($form_state['storage']['confirm'])) {
    // read the file
    $file = file_load($form_state['values']['csv']);
    $csv = array();
    $lines = file($file->uri, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    foreach ($lines as $value) {
      $value = str_getcsv($value);

      if(!empty($value) && $value[0] != NULL && $value[0] != '') {
        $csv[$value[0]] = $value[1] ? $value[1] : '0.00';
      }
    }

    $form_state['storage']['values'] = $form_state['values'];
    $form_state['storage']['values']['postcodes'] = $csv;
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  } else {
    foreach($form_state['storage']['values']['postcodes'] as $postcode => $price) {
      db_merge('pab_postcodes')
        ->key(array('postcode' => strtolower($postcode)))
        ->fields(array(
            'price' => $price * 100,
        ))
        ->execute();
      }

    $form_state['redirect'] = 'admin/pab/postcode';
  }
}

function pab_shipping_get_postcodes() {
  $postcodes = array();
  $query = db_select('pab_postcodes', 'p');
  $query->orderBy('p.postcode');
  $query->fields('p');
  $results = $query->execute()->fetchAll();
  foreach($results as $result) {
    $postcodes[$result->postcode] = strtoupper($result->postcode);
  }
  return $postcodes;
}

// TO DO
function pab_shipping_postcode_lookup($postcode) {
  $price = 0;

  $query = db_select('pab_postcodes', 'p');
  $query->orderBy('p.postcode');
  $query->fields('p');
  $results = $query->execute()->fetchAll();
  foreach($results as $result) {
    $postcodes[$result->postcode] = $result->postcode;
  }

  foreach($postcodes as $id => $p) {
    if (strpos($postcode, $id) === 0) {
      return TRUE;
    }
  }

  return FALSE;
}

// TO DO
function pab_shipping_postcode_price_lookup($postcode) {
  $price = 0;

  $postcodes = array();
  $query = db_select('pab_postcodes', 'p');
  $query->orderBy('p.postcode');
  $query->fields('p');
  $results = $query->execute()->fetchAll();
  foreach($results as $result) {
    $postcodes[$result->postcode] = $result->price;
  }

  foreach($postcodes as $id => $p) {
    if (strpos($postcode, $id) === 0) {
      $price = $p;
    }
  }

  if($price > 0) {
    return $price;
  }

  return 0;
}

/**
 * Implements hook_commerce_shipping_method_info().
 */
function pab_shipping_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['pab_shipping_method'] = array(
    'title' => t('Postal Code shipping method'),
    'description' => t('Defines a single flat rate service with a couple of service details.'),
  );

  return $shipping_methods;
}

/**
 * Implements hook_commerce_shipping_service_info().
 */
function pab_shipping_commerce_shipping_service_info() {
  $shipping_services = array();
  $shipping_services['pab_shipping_service'] = array(
    'title' => t('Postal Code shipping service'),
    'description' => t('Delivery charge to deliver to your postcode.'),
    'display_title' => t('Delivery Charge'),
    'shipping_method' => 'pab_shipping_method',
    'price_component' => 'delivery',
    'callbacks' => array(
      'rate' => 'pab_shipping_service_rate',
    ),
  );

  return $shipping_services;
}

function pab_shipping_service_rate($shipping_service, $order) {
  $price = 0;
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  if(isset($order->commerce_customer_shipping) && isset($order->commerce_customer_shipping[LANGUAGE_NONE])) {
    $shipping_profile = entity_metadata_wrapper('commerce_customer_profile', $order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']);
    $postcode = strtolower(str_replace(' ', '', $shipping_profile->value()->commerce_customer_address[LANGUAGE_NONE][0]['postal_code']));
    $price = pab_shipping_postcode_price_lookup($postcode);
  } else {
    foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      if($line_item_wrapper->type->value() != 'product') {
        continue;
      }
      $price = pab_shipping_postcode_price_lookup($line_item_wrapper->field_post_code->value());
    }
  }
  $default_currency = commerce_currency_load();
  return array(
    'amount' => $price,
    'currency_code' => $default_currency['code'],
    'data' => array(),
  );
}

function pab_shipping_commerce_checkout_pane_info_alter(&$checkout_panes) {
  if(isset($checkout_panes['customer_profile_shipping'])){
    $checkout_panes['customer_profile_shipping']['title'] = t('Delivery Information');
  }
  if(isset($checkout_panes['commerce_shipping'])){
    $checkout_panes['commerce_shipping']['title'] = t('Delivery');
    $checkout_panes['commerce_shipping']['callbacks']['checkout_form_validate'] = 'pab_shipping_commerce_shipping_pane_validate';
  }
}

function pab_shipping_commerce_shipping_pane_validate(&$form, &$form_state, $checkout_pane, $order) {
  // check they added a valid post code
  $shipping_profile = entity_metadata_wrapper('commerce_customer_profile', $order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']);
  $postcode = strtolower(str_replace(' ', '', $shipping_profile->value()->commerce_customer_address[LANGUAGE_NONE][0]['postal_code']));
  if(!pab_shipping_postcode_lookup($postcode)) {
    drupal_set_message(t('Sorry we do not currently deliver to your postcode.'), 'error');
    return FALSE;
  }

  return TRUE;
}
